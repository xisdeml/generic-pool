﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Pool_Base : MonoBehaviour
{
    private event Action _onDisable;

    public virtual void Disable() => _onDisable?.Invoke();

    public void OnInstantiate(Action onDisable) => _onDisable = onDisable;
}
