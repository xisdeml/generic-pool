﻿using System.Collections.Generic;
using UnityEngine;
using System;

public class GenericPool <T> where T : Pool_Base
{
    public static GenericPool<T> CreateNewPool(T poolObject, Transform container)
    {
        GenericPool<T> pool = new GenericPool<T>();
        pool._poolObject = poolObject;
        pool._container = container;
        return pool;
    }

    [SerializeField] protected T _poolObject;

    [SerializeField] protected Transform _container;

    private List<T> _list = new List<T>();

    private Queue<T> _disabledObjects = new Queue<T>();

    private Action<T> _onDisable;

    public void SetOnDeactive(Action<T> onDisable)
    {
        _onDisable = onDisable;
    }

    public T New()
    {
        T obj;

        if (_disabledObjects.Count > 0)
            obj = _disabledObjects.Dequeue();
        else
        {

            obj = UnityEngine.Object.Instantiate(_poolObject, _container);

            obj.OnInstantiate(
                () => 
                {
                    _disabledObjects.Enqueue(obj);
                    _list.Remove(obj);
                    _onDisable?.Invoke(obj);
                });

            _list.Add(obj);
        }

        return obj;
    }

    public void DisableeAll()
    {
        foreach (var obj in _list)
        {
            obj.Disable();
        }
    }
}
